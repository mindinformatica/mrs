package mrs

import "net/http"

//Action ...
type Action struct {
	Path       string
	Method     string
	ActionFunc func(w http.ResponseWriter, r *http.Request)
}
