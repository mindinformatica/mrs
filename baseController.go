package mrs

//BaseController ...
type BaseController struct {
	Path    string
	actions []Action
}

//Controller ...
type Controller interface {
	GetActions() []Action
	AddAction(action Action)
	GetPath() string
}

//GetActions ...
func (controller *BaseController) GetActions() []Action {
	return controller.actions
}

//AddAction ...
func (controller *BaseController) AddAction(action Action) {
	controller.actions = append(controller.actions, action)
}

//GetPath ...
func (controller *BaseController) GetPath() string {
	return controller.Path
}
