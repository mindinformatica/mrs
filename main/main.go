package main

import "bitbucket.org/mindinformatica/mrs"

func main() {
	var c = mrs.Conf{
		DbDialect:    "postgres",
		DbConnection: "",
	}
	mrs.SetConfiguration(c)
}
