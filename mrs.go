package mrs

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//Conf ...
type Conf struct {
	DbDialect    string
	DbConnection string
	EnableCors   bool
}

var mrsConf Conf

//SetConfiguration ...
func SetConfiguration(conf Conf) {
	mrsConf = conf
}

//Db ....
func Db() (*gorm.DB, error) {
	if mrsConf.DbDialect == "" {
		panic("non è stato impostato nessun dialect per la connessione al database")
	}
	if mrsConf.DbConnection == "" {
		panic("non è stata impostata nessuna stringa di connessione al database")
	}
	return gorm.Open(mrsConf.DbDialect, mrsConf.DbConnection) //"postgres", "host=localhost port=5432 user=postgres dbname=provago password=slmind sslmode=disable")
}

//RegisterController ...
func RegisterController(controller Controller, router *mux.Router) {
	for _, action := range controller.GetActions() {
		router.HandleFunc(controller.GetPath()+action.Path, prepareRestActionFunc(action.ActionFunc)).Methods(action.Method)
	}
}

func prepareRestActionFunc(f func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if mrsConf.EnableCors {
			w.Header().Set("Access-Control-Allow-Origin", "*")
		}
		w.Header().Set("Content-Type", "application/json")
		f(w, r)
	}
}

/*func main() {
	router.HandleFunc("/ciao", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ciao")
	})
	db, err := config.Db()
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&controllers.Product{})
	db.AutoMigrate(&libri.Libro{})
	var c = controllers.BaseController{Path: "azioni/"}
	c.AddAction(actions.Action{Path: "uno", Method: "GET", ActionFunc: func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ma che cazzo")
	}})

	c.AddAction(actions.Action{Path: "due/", Method: "GET", ActionFunc: func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ciack!!")
	}})

	registerController(&c, router)

	var rc = controllers.ProductController{Path: "/products"}

	registerController(&rc, router)

	registerController(&libri.LibroController{Path: "/libri"}, router)

	log.Fatal(http.ListenAndServe(":8080", router))
}*/
